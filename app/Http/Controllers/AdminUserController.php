<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Notifications\UserActivationNotification;
class AdminUserController extends Controller
{
    //
	public function showUser()
    {
		$users = User::where([
			'is_admin' => "0"
		])->get();
        
		return view('admin.users')->with(compact('users'));
    }

    public function approveUser($id)
    {
    	$user = User::find($id);
    	if(!$user->isActivatedUser())
    	{
    		$user->status = "1";
    		$user->save();
    		$user->notify(new UserActivationNotification($user));
    		return redirect()->back()->with('success','Your account have been activated!');
		}
		return redirect()->back()->with('error','Your account have not been activated!');
	}
}
