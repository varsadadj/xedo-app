<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function getStateByCountryId($countryId)
    {
        $states = Country::find($countryId)->states;
        return \Response::json($states);
    }
    public function sendEmail()
    {
        \Mail::send('emails.register', ['name' => "Dhaval", 'content' => "test content"], function ($message)
        {

            $message->from('me@gmail.com', 'Christian Nwamba');

            $message->to('dhavaltspl@gmail.com');

        });

        return response()->json(['message' => 'Request completed']);
    }
}
