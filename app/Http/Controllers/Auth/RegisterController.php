<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Country;
use App\Notifications\UserRegistered;
use Illuminate\Http\Request;
use App\Mail\WelcomeEmail;
//use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Session;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function showRegistrationForm()
    {
        $countries = Country::all(['id','name'])->toArray();
        /*echo "<pre>";
        print_r($countries);*/
        return view('auth.register',['countries' => $countries]);
    }    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'business_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'country_id' => 'required',
            'state_id' => 'required',
            'city' => 'required|string|max:255',
            'postal_code' => 'required|string|max:6',
            'mobile_phone' => 'required|string|max:14',
            'agree' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        Session::flash('success', 'Thank you! Your account has been successfully registered.');
        //print_r($data);die;
        $user = User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'business_name' => $data['business_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'country_id' => $data['country_id'],
            'state_id' => $data['state_id'],
            'city' => $data['city'],
            'postal_code' => $data['postal_code'],
            'mobile_phone' => $data['mobile_phone'],
            /*'abn' => 'test',
            'street_number' => '301',
            'street_name' => 'test street',*/
        ]);
        //Mail::to($data['email'])->send(new WelcomeEmail($user));
        return $user;

    }
    /**
     * [registered email notify to use]
     * @param  Request $request [description]
     * @param  [type]  $user    [description]
     * @return [type]           [description]
     */
    protected function registered(Request $request, $user) {
        $user->notify(new UserRegistered($user));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }
}
