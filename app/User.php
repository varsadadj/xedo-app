<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'business_name', 'email', 
        'password','country_id', 'state_id','mobile_phone', 
        'city','postal_code','abn','street_number','street_name',''
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        if($this->is_admin == "1")
        {
            return true;
        }
        return false;
        //return $this->admin; 
        // this looks for an admin column in your users table
    }

    public function isActivatedUser()
    {
        if($this->status == "1")
        {
            return true;
        }
        return false;
    }

    public function state()
    {
        return $this->hasOne('App\State','id','state_id');
    }

    public function country()
    {
        return $this->hasOne('App\Country','id','country_id');
    }
}
