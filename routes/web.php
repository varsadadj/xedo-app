<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'Auth\RegisterController@showRegistrationForm');
//Login and Register routes by Authentication Scaffolding.
Auth::routes();
//After normal user redirected on home page
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/get-state-by-country/{id}', 'HomeController@getStateByCountryId')->name('get_state_by_country');
//Route::get('/send/1', 'HomeController@sendEmail');

Route::get('user/dashboard', ['middleware' => ['auth'], function() {
    return "this page requires that you be logged in";
}]);

//Admin routes
Route::middleware(['auth', 'admin'])->group(function(){
	Route::get('admin/dashboard', 'AdminUserController@showUser')->name('admin_dashboard');
	Route::get('admin/approve/{id}', 'AdminUserController@approveUser')->name('admin_user_approve');
});
