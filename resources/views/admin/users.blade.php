@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">User List</div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Business Name</th>
                                <th>Business Email</th>
                                <th>Phone</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Country</th>
                                <th>Created On</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                                <td>{{ $user->business_name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->mobile_phone }}</td>
                                <td>{{ $user->city }}</td>
                                <td>{{ $user->state->name }}</td>
                                <td>{{ $user->country->name }}</td>
                                <td>{{ $user->created_at->format('d-m-Y') }}</td>
                                @if($user->status == "1")
                                    <td><a href="#" class="btn btn-success btn-xs disabled" >Approved <i class="fa fa-check-circle"></i></a></td>
                                @else
                                    <td><a href="{{ route('admin_user_approve',[$user->id]) }}" class="btn btn-success btn-xs approve-user" >Approve <i class="fa fa-check-circle"></i></a></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript_block')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.approve-user').on('click', function(e){
            if(confirm("Are you sure you want to activate this user?")){
                console.log($(this).attr('href'));
                return true;
            }
            else
            {
                return false;
            }
        });
    });
</script>
@endsection